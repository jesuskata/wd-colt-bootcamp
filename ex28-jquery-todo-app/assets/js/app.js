//Marcar todos específicos al click-ear
$("ul").on("click", "li", function() { //Este cambio se hizo debido a que si solo sustituimos el click por on seguimos haciendo la petición sobre los li existentes desde el inicio del programa. Pero agregando el argumento li al seleccionar ul, entonces ya estamos previniendo los click a todos los li contenidos en el ul
    $(this).toggleClass("completed");
/*    //if li es gris
    if($(this).css("color") === "rgb(128, 128, 128)") {
        //Cambiar a negro
        $(this).css({
            color: "black",
            textDecoration: "none"
        });
   } else {
        //Cambiar a gris y subrayarlo
        $(this).css({
            color: "gray",
            textDecoration: "line-through"
        });
   }*/
});

/*NOTA! Bubble*/

$("ul").on("click", "span", function(event) { //Igual que en el on() anterior, estamos anticipando el click sobre los span contenidos en ul
    //Con esta instrucción se elimina la selección y elemento que lo contiene (parent)
    //$(this).parent().remove();
    //Con esta instrucción se oculta la selección y elemento que lo contiene (parent)
    //$(this).parent().fadeOut(); //No nos interesa guardar los datos ocultos para siempre
    //Así que se agrega la instrucción remove
    //$(this).parent().fadeOut().remove(); //Hay un detalle, al hacer esto el remove no permitirá hacer el fadeOut, sino solo eliminará sin ningún efecto visual
    //Por lo tanto, remove lo metemos en una función dentro del fadeOut
    $(this).parent().fadeOut(500, function() {
        //Este this hace referencia al parent(), por lo tanto este this es diferente al anterior que hace referencia al span
        $(this).remove();
    });
    //Esto evita que se propague el "bubble" a los demás elementos que contienen el elemento seleccionado
    event.stopPropagation();
});

$("input[type='text']").keypress(function(event) {
    //Recordemos que el valor de which es el de la tecla específica que haya presionado
    if(event.which === 13) {
        //Vamos a guardar el texto que se escriba en el input en una variable para posteriormente usarla
        var todoText = $(this).val(); //La instrucción val no regresa el valor del elemento que lo contiene
        $(this).val("");
        //Vamos a agregar el texto escrito en el input a la lista
        $("ul").append("<li><span><i class='fa fa-trash-o' aria-hidden='true'></i></span> " + todoText + "</li>");
        //NOTA! Un dato curioso, los nuevos elementos no pueden ser eliminados, debido al uso de click() en vez de on(), como lo explicó en capítulos anteriores: click() only adds listeners for existing elements; on() will add listeners for all potencial future elements
    }
});

$(".fa-plus").click(function() {
    $("input[type='text']").fadeToggle();
})