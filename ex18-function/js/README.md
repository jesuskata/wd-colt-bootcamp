## Con esto evaluamos la ecuación y nos devuelve un valor booleano
return x % 2 === 0;

## Se reduce result = result * i con la siguiente forma
result *= i;

## Se usó el flag 'g' que significa global
str.replace(/-/g , "_");

## Llamando a otra función (no se usan los paréntesis en el llamado de la función)
setInterval(anotherFunction, interval)
clearInterval(number)

## Usando una función anónima
setInterval(function(){
}, interval)