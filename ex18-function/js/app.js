/*function isEven(x) {
    //return true is even
    if(x % 2 === 0) {
        return(true)
    //return false is not even    
    }else {
        return(false)
    }
}*/

function isEven(x) {
    return x % 2 === 0;
}

function factorial(num) {
    var result = 1;
    for (var i = 2; i <= num; i++)
        result *= i;
        return result;
}

function kebabToSnake(str) {
    var newStr = str.replace(/-/g , "_");
    return newStr;
}