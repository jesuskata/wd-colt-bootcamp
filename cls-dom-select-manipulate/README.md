# DOM Select and Manipulate
En este documento veremos algunos de los componentes que se muestran en la clase de Colt

1. Lo primero que vemos es una de las formas de seleccionar algo dentro del documento; en este caso con `documen.querySelector`
```javascript
var h1 = document.querySelector("h1");
```

2. Ahora se denota la forma de manipular dicho elemento seleccionado y guardado en una variable con `.style.color`
```javascript
h1.style.color = "pink";
```

3. En el siguiente ejemplo veremos el uso de una función que habíamos visto en otra clase `setInterval`, con el cual podemos hacer que un elemento tenga intervalos a traves de dos parámetros; el primero es el elemento a manipular, y el segundo es la cantidad de tiempo en milisegundos para que tenga los intervalos
```javascript
setInterval(function() {
    if(isBlue) {
        body.style.background = "white";
    }else {
        body.style.background = "#3498db";
    }
    isBlue = !isBlue;
}, 1000);
```

4. Ahora tenemos las siguientes líneas que podemos colocar en la consola de Chrome y ver qué es lo que nos muestra:
```javascript
document.URL;
document.head;
document.body;
document.links;
```
5. El `document` viene por default con muchos métodos para seleccionar elementos. Algunos de los más importantes son:
```javascript
document.getElementById()
document.getElementsByClassName()
document.getElementsByTagName()
document.querySelector()
document.querySelectorAll()
```