//Vamos a seleccionar los elementos según su id
var tag = document.getElementById("highligth");
//Para verificar el contenido de highlight
console.dir(tag);

//Vamos a seleccionar los elementos según su class
var tags = document.getElementsByClassName("bolded");
//Para verificar el contenido de una de las clases
console.dir(tags[0]);
//Este elemento puede verificarse su contenido con
tags[0];
//Además, podemos saber su longitud o número de elementos que contiene como objeto
tags.length;

//Ahora vamos a seleccionar los tres elementos de la lista a través de su tag li
var tags = document.getElementsByTagName("li");
console.log(tags);

var body =  document.getElementsByTagName("body")[0];

//A continuación veremos el uso de querySelector, en el cuál veremos que puede seleccionar todo lo que los anteriores pueden seleccionar, pero con la única diferencia que solamente selecciona el primer elemento con las condiciones que le establezcamos
//Selección por id
var tag = document.querySelector("#highligth");
//Selección por class
var tag = document.querySelector("#bolded"); //selecciona el primer class bolded
//Selección por tag
var tag = document.querySelector("h1"); //selecciona el primer h1

//El siguiente selector nos selecciona todos los elementos contenidos en la condición que se le indique
//Selección por id
var tag = document.querySelectorAll("#highligth");
//Selección por class
var tag = document.querySelectorAll("#bolded"); //selecciona todos los elementos de class bolded
//Selección por tag
var tag = document.querySelectorAll("h1"); //selecciona todos los elementos de tag h1
