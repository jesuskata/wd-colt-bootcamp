$("h1")

$("li")

$("a")

$("h1").css("color", "yellow")

var styles = {
    color: "red",
    background: "pink",
    border: "2px solid purple"
}

styles

$("h1").css(styles)

$("li").css("color", "blue")

var lis = document.querySelectorAll("li")

for(var i = 0; i < lis.length; i++) {
    lis[i].style.color = "orange";
}

$("li").css("color", "blue")

$("a").css("font-size", "40px")

$("li").css({
    fontSize: "10px",
    border: "3px dashed purple",
    backgroundColor: "rgba(89, 56, 88, 0.5)"
});