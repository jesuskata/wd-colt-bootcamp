/*var reverseNum = [1, 2, 3, 4];
var reverseStr = ["a", "b", "c"];

//console.log("Imprime en reversa el Array numérico")
console.log("----------");
for(var i = reverseNum.length; i >= 0; i--) {
    console.log(reverseNum[i]);
}
console.log("----------");

//console.log("Imprime en reversa el Array de texto")
console.log("----------");
for(var i = reverseNum.length; i >= 0; i--) {
    console.log(reverseStr[i]);
}
console.log("----------");*/

function printReverse(array) {
    for(var i = array.length - 1; i >= 0; i--)
    console.log(array[i]);
}
printReverse([1, 2, 3, 4]);

function isUniform(array) {
    var first = array[0];
    for(var i = 1; i < array.length; i++) {
        if(array[i] !== first) {
            return false;
        }
    }
    return true;
}

function sumArray(array) {
    var total = 0;
    array.forEach(function(element) {
        total += element;
    });
    return total;
}

function max(array) {
    var max = array[0];
    for(var i = 1; i < array.length; i++) {
        if(array[i] > max) {
            max = array[i];
        }
    }
    return max;
}