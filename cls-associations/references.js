var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/blog_demo_2");

var Post = require("./models/post")
var User = require("./models/user")

/*Post.create({
    title: "Cómo Hacer un Hello World Magnífico Pt. 4",
    content: "Muy bien hecho"
}, function(err, post) {
    User.findOne({email: "hello@world.com"}, function(err, foundUser) {
        if(err) {
            console.log(err);
        } else {
            foundUser.posts.push(post);
            foundUser.save(function(err, data) {
                if(err) {
                    console.log(err);
                } else {
                    console.log(data);
                }
            });
        }
    });
});*/

//find user
//find all posts for that user

User.findOne({email: "hello@world.com"}).populate("posts").exec(function(err, user) {
    if(err) {
        console.log(err);
    } else {
        console.log(user);
    }
});