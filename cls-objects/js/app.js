var person = {
    name: "Alex",
    age: 31,
    city: "Huajuapan de León"
};

//Existen dos formas de adquirir los valores
console.log(person.age) //31
console.log(person["age"]) //31

//Existen algunas diferencias entre ambas maneras de llamar los datos
//No puedes usar la notación de punto si la propiedad comienza con número
someObject.1blah //invalid
someObject["1blah"] //valid

//Puedes buscar usando una variable solo con la notación de brackets
var str = "name";
someObject.str //no busca "name"
someObject[str] //evalúa str y busca por "name"

//No puedes usar notación de punto para propiedades con espacio intermedio
someObject.fav color //invalid
someObject["fav color"] //valid

//Podemos actualizar los datos de la siguiente manera
var person = {
    name: "Alex",
    age: 31,
    city: "Huajuapan de León"
};

//Para actualizar la edad
person["age"] += 1;
//Para actualizar la ciudad
person.city = "Oaxaca";

//Existen diferentes maneras de crear un objeto
//Haciendo un objeto vacío, entonces se agregan a este las propiedades
var person = {}
person.name = "Travis";
person.age = 21;
person.city = "LA";

//Todo a la primera
var person = {
    name: "Travis",
    age: 21,
    city: "LA"
}

var person = new Object();
person.name = "Travis";
person.age = 21;
person.city = "LA";

//Los objetos pueden incluir todo tipo de datos, incluidos otros objetos
var junkObject = {
    age: 21,
    color: "purple",
    isHungry: true,
    friends: ["Elo", "Lupe"],
    mascota: {
        name: "Trompis",
        species: "Tortuga",
        age: 3
    }
}