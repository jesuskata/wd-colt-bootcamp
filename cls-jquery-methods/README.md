# Clase: jQuery Methods
En esta clase veremos 7 metodos comunes de uso en jQuery

- val()
Nos devuelve el valor del elemento seleccionado
- text()
Tiene un funcionamiento parecido al de textContent de js. Dentro de los paréntesis, lo que nosotros escribamos lo va a sustiruir por el contenido previo del elemento seleccionado
- attr()
Tiene la misma funcionalidad que getAttribute y setAttribute
- html()
Tiene un funcionamiento similar a innerHTML de js
- addClass()
Tiene una funcionalidad como classList.add de js
- removeClass()
Tiene una funcionalidad como classList.remove de js
- toggleClass()
Tiene una funcionalidad como classList.toggle de js