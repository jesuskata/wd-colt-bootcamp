var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/cat_app");
mongoose.Promise = global.Promise;

var catSchema = new mongoose.Schema({
    name: String,
    age: Number,
    temperament: String
});

var Cat = mongoose.model("Cat", catSchema);

//Adding a new cat to the DB
// var george = new Cat({
//     name: "George",
//     age: 11,
//     temperament: "Very-Bad"
// });

// george.save(function(err, cat){
//     if(err) {
//      console.log("Algo salió mal!");
//     } else {
//      console.log("Hemos guardado un gato a la DB!");
//      console.log(cat);
//     }
// });

//Otro método para Adding a new cat to the DB

Cat.create({
    name: "Snow White",
    age: 15,
    temperament: "Bland"
}, function(err, cat) {
    if(err) {
        console.log("Error!");
    } else {
        console.log(cat);
    }
});

//Retrieve all cats from the DB and console.log each oane

Cat.find({}, function(err, cats) {
    if(err) {
     console.log("Error!");
     console.log(err);
    } else {
     console.log("Todos los gatos:");
     console.log(cats);
    }
});