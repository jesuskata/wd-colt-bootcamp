# Databases

## Intro to Databases

* What is a database?
    * A collection of information/data
    * Has an interfase
* SQL(relational) vs. NoSQL(non-relational)

## Intro to MongoDB

* What is MongoDB
* Why are we using it?
* Let's install 

## Our First Mogo Commands

* mongod
* mongo
* help
* show dbs
* use
    * use dogs
* db //Muestra la base de datos en la que estamos colocados
* insert
    * db.dogs.insert({})
* show collections
* find
    * db.dogs.find() //Busca a todos los id dados de alta
    * db.dogs.find({name: "Lulu"})
* update
    * db.dogs.update({name: "Lulu"}, {breed: "Labradoodle"}) //Cambia a Lulu el breed, pero no matiene la información anterior
    * db.dogs.update({name: "Rusty"}, {$set: {name: "Tater", isCute: true}}) //Modifica el nombre, agrega el valor isCute, pero mantiene la demás información previa que no se haya modificado
* remove
    * db.dogs.remove({breed: "Labradoodle"}) //Elimina el concepto que se relacione con lo solicitado, y todos los que sean similares
    * db.dogs.remove({breed: "Labradoodle"}).limit(1) //Elimina el concepto que se relacione con lo solicitado, y el número de elementos indicado en limit
* NOTA! CRUD: Por sus siglas significa: create(insert), read(find), update and delete

## Mongoose

* What is Mongoose?
* Why are we using it?
* Interact with a Mongo Database using Mongoose