var numSquares = 6;
var colors = [];
var pickedColor;
var square = document.getElementsByClassName("square");
var colorDisplay = document.getElementById("colorDisplay");
var messageDisplay = document.getElementById("message");
var h1 = document.querySelector("h1");
var resetButton = document.querySelector("#reset");
var modeButton = document.querySelectorAll(".mode");

init();

function init() {
    setupModeButtons();
    setupSquares();
    reset();
}

function setupModeButtons() {
    //Botones de mode eventListeners
    for(var i = 0; i < modeButton.length; i++) {
        modeButton[i].addEventListener("click", function() {
            modeButton[0].classList.remove("selected");
            modeButton[1].classList.remove("selected");
            this.classList.add("selected");
            this.textContent === "Fácil" ? numSquares = 3: numSquares = 6;
            reset();
        });
    }
}

function setupSquares() {
    for(var i = 0; i < square.length; i++) {
    //Agregar un Listener tipo click a cada cuadro
        square[i].addEventListener("click", function() {
            //Obtener el color del cuadro click-eado
            var clickedColor = this.style.backgroundColor;
            //Comparar el color con el solicitado en el h1
            if(clickedColor === pickedColor) {
                messageDisplay.textContent = "Correct!";
                resetButton.textContent = "Jugar de Nuevo?";
                changeColors(clickedColor);
                h1.style.backgroundColor = clickedColor;
            }else {
                this.style.backgroundColor = "#232323";
                messageDisplay.textContent = "Wrong!, Keep Trying";
            }
        })
    }
}

function reset() {
    colors = generateRandomColors(numSquares);
    //Tomar un nuevo color del arreglo
    pickedColor = pickColor();
    //Cambiar el colorDisplay para pedir el color a click-ear
    colorDisplay.textContent = pickedColor;
    //Cambiar los colores de los cuadros
    for(var i = 0; i < square.length; i++) {
        if(colors[i]) {
            square[i].style.display = "block";
            square[i].style.backgroundColor = colors[i];
        }else {
            square[i].style.display = "none";
        }
        square[i].style.backgroundColor = colors[i];
    }
    //Quitar el mensaje
    messageDisplay.textContent = "";
    h1.style.backgroundColor = "steelblue";
    resetButton.textContent = "Nuevos Colores";    
}

resetButton.addEventListener("click", function() {
    reset();
})

function changeColors(color) {
    //Loop para todos los cuadros
    for(var i = 0; i < square.length; i++) {
        //Cambiar cada color según el color requerido
        square[i].style.backgroundColor = color;
    }
}

function pickColor() {
    var random = Math.floor(Math.random() * colors.length);
    return colors[random];
}

function generateRandomColors(num) {
    //Hacer un arreglo
    var arr = [];
    //Hacer un arreglo que agregue "num" de colores random
    for(var i = 0; i < num; i++) {
        //Obtener color random y enviarlo al arreglo arr
        arr.push(randomColor());
        //arr[i] = randomColor();
    }
    //Regresar arreglo
    return arr;
}

function randomColor() {
    //Tomar un "red" de 0 - 255
    var r = Math.floor(Math.random() * 256);
    //Tomar un "green" de 0 - 255
    var g = Math.floor(Math.random() * 256);
    //Tomar un "blue" de 0 - 255
    var b = Math.floor(Math.random() * 256);
    return "rgb(" + r + ", " + g + ", " + b + ")";
}