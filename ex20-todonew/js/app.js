var todos = [];

var input = prompt("Qué te gustaría hacer?");

while(input !== "quit") {
    if(input === "list") {
        listTodos();
    }else if(input === "new") {
        addTodo();
    }else if(input === "delete") {
        deleteTodo();
    }
    //preguntar de nuevo
    var input = prompt("Qué te gustaría hacer?");
}
console.log("Ok, hemos terminado por hoy");

function listTodos() {
    console.log("----------");
    todos.forEach(function(todo, i) {
        console.log(i + ": " + todo);
    });
    console.log("----------");
}

function addTodo() {
    //pedir un nuevo todo
    var newTodo = prompt("Ingresa un nuevo Todo");
    //agregar al arreglo todo
    todos.push(newTodo);
    console.log("Se ha agregado el todo exitosamente");
}

function deleteTodo() {
    //preguntar por el índice de todo a eliminar
    var index = prompt("Ingresa el índice elemento que desea eliminar");
    //eliminar ese todo
    todos.splice(index,1);
    console.log("Se ha eliminado el elemento " + index + " exitosamente");
}