# Ejercicio: ToDo New
En este ejercicio podemos ver que se ha completado el ToDo List de las clases anteriores, agregando una opción delete. Vamos a ver dos nuevos elementos que se utilizaron para lograrlo

1. El primero de ellos es el uso de `forEach` para extraer la lista completa que se tiene en el arreglo. Pero se agregó un elemento importante más
```javascript
function listTodos() {
    console.log("----------");
    todos.forEach(function(todo, i) {
        console.log(i + ": " + todo);
    });
    console.log("----------");
}
```

Como podemos observar, `forEach(todo, i)` contiene dos argumentos, el primero `todo`  es para imprimir o extraer el elemento en sí del arreglo; el segundo argumento `i` hace referencia al índice en el cuál se encuentra el elemento extraído (número)

2. El segundo elemento que se utilizó fue el `splice()`
```javascript
function deleteTodo() {
    //preguntar por el índice de todo a eliminar
    var index = prompt("Ingresa el índice elemento que desea eliminar");
    //eliminar ese todo
    todos.splice(index,1);
    console.log("Se ha eliminado el elemento " + index + " exitosamente");
}
```

Con este nuevo elemento lo que logramos es eliminar el elemento que requiramos en el primer argumento `index`, y el segundo argumento utilizado en `splice` es la cantidad de elementos a eliminar del `array`

## Por el momento es todo, hemos aprendido a usar dos nuevas herramientas en JavaScript