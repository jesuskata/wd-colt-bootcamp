/*$("h1").click(function() {
    alert("h1 was clicked")
})*/

/*$("button").click(function() {
    alert("Button clicked")
})*/

/*$("button").click(function() {
    var text = $(this).text();
    console.log("You clicked " + text);
    $(this).css("background", "pink");
})*/

//Keypress
/*$("input").keypress(function() {
    console.log("Has presionado una tecla");
})*/

//El nombre del parámetro puede ser cualquiera (event, e, hola, etc.)
/*$("input").keypress(function(event) {
    //Podemos identificar en la consola el número que corresponde a la letra del teclado que hemos presionado, o tembién existe material interesante en https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
    if(event.which === 13) {
        alert("Has presionado ENTER")
    }
    console.log(event);
})*/

$("h1").on("click", function() {
    //$("h1").css("color", "purple");
    $(this).css("color", "purple");
})

$("input").on("keypress", function() {
    console.log("Keypressed!")
})

$("button").on("mouseenter", function() {
    //$(this).toggleClass("bold");
    $(this).css("font-weight", "bold");
})

$("button").on("mouseleave", function() {
    $(this).css("font-weight", "normal");
})