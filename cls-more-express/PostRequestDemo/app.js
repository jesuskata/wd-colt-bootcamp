var express = require("express");
var app = express();
var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
var friends = ["Eloísa", "Aleisa", "Lupe", "Jaime", "Omar"];

app.get("/", function(req, res) {
    res.render("home");
});

app.post("/addfriend", function(req, res) {
    var newFriend = req.body.newfriend;
    friends.push(newFriend);
    //res.send("Has alcanzado la ruta de posts!!!")
    res.redirect("/friends"); //Con esta nueva instrucción lo que hacemos es redirigir (una vez hecha la acción) a la ruta que determinemos
});

app.get("/friends", function(req, res) {
    res.render("friends", {friends: friends}); //NOTA! El primer friends puede ser cualquier nombre que deseemos, el segundo se refiere a la variable declarada anteriormente llamada friends
});

//app.listen(process.env.PORT, process.env.IP, function() {
app.listen(3000, function() {
    console.log("Server inicializado");
});