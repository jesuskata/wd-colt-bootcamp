## Rendering HTML and Templates

* Use res.render() to render HTML (from an EJS file)
    * Es importante mencionar que para que esto funcione, es necesario instalar el paquete EJS `npm install ejs --save`, sino, el archivo no va a poder ser leído correctamente (revisar el archivo creado package.json)
    * Estos archivos van a ser leídos desde la carpeta llamada `views`, que se encuentra dentro de la carpeta contenedora del achivo app.js
* Explain what EJS is and why we use it
    * De su traducción Embebed JavaScript, este sustituye a los archivos html, debido a que en express debemos incluir html dinámico y esa es la función del archivo con extensión ejs. Además, este tipo de archivo permite incluir Javascript de manera directa en las etiquetas html con la instrucción `<%= %>`
* Pass variables to EJS templates

## EJS Control Flow

* Show examples of control flow in EJS templates
    * Cuando se usa `<%= %>` es porque JavaScript va a ejecutar una operación simple dentro de html
    * Cuando se usa `<% %>` es porque JavaScript va a ejecutar una operación lógica y no necesariamente dentro de html; además, este es colocado en cala línea donde se ejecute JavaScript, abriendo y cerrando en cada línea la condición
* Write if statements in an EJS file
* Write loops in an EJS file

## Styles And Partials

* Show how to properly include public assets
* Properly configure our app to use EJS
* Use partials to dry our code!

## Post Request

* Write post routes, and test them with Postman
* Use a form to send a post request
* Use body parser to get from data