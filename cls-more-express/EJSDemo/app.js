var express = require('express');
var app = express();

//Con esta instrucción, establecemos la carpeta donde se leerán los archivos de estilo css
app.use(express.static("public"));
//Con esta instrucción anticipamos que los archivos a llamar serán .ejs, por lo tanto, ya no es necesario que index, love y posts lleven la extensión .ejs
app.set("view engine", "ejs")

app.get("/", function(req, res) {
    res.render("index");
    //res.send("<h1>Bienvenido a la página inicial</h1>");
});

app.get("/fallinlovewith/:thing", function(req, res) {
    var thing = req.params.thing;
    res.render("love", {thingVar: thing});
    //res.send("You fell in love with " + thing);
});

app.get("/posts", function(req, res) {
    var posts = [
        {title: "Los bebés son maravillosos", author: "Eloisa"},
        {title: "Uh Ah es un regalo de mi tío Julio", author: "Jesús"},
        {title: "Necesito mejorar mi comportamiento", author: "Aleisa"}
    ]
    res.render("posts", {posts: posts});
})

//app.listen(process.env.PORT, process.env.IP, function() {
app.listen(3000, function() {
    console.log("El servidor se ha inicializado");
})