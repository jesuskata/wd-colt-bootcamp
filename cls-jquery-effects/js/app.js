/*//Esto funciona para desaparecer algo que está en display block
$("button").on("click", function() {
    //Con este ejemplo, desde el momento que click-eamos el console.log entra en acción
    //$("div").fadeOut(1000);
    //console.log("Fade completed");
    //Con este ejemplo, desde el momento que click-eamos el console.log entra en acción
    $("div").fadeOut(1000, function () {
        //Con la siguiente intrucción, eliminamos el elemento seleccionado del DOM
        $(this).remove();
        //console.log("Fade completed");
    });
})*/

/*//Con este método hacemos que algo que está oculto (display none) se revele
$("button").on("click", function() {
    $("div").fadeIn(1000);
})*/

/*//Con este método conseguimos que cada que se click-ee aparezca y desaparezca el contenido
$("button").on("click", function() {
    $("div").fadeToggle(1000);
})*/

//Estos efectos nos sirven para mostrar u ocultar elementos con efectos de deslice de arreiba a abajo y viceversa
$("button").on("click", function() {
    //$("div").slideDown(1000);
    //slideUp solo funciona en elementos que están visibles
    //$("div").slideUp(1000);
    $("div").slideToggle(1000);
})