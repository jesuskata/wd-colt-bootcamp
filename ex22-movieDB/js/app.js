var movies = [
    {
        titulo: "Harry Potter: Y La Piedra Filosofal",
        watched: true,
        stars: 4.5
    },
    {
        titulo: "Venciendo Gigantes",
        watched: true,
        stars: 5
    },
    {
        titulo: "A Prueba de Fuego",
        watched: true,
        stars: 5
    },
    {
        titulo: "New Spiderman",
        watched: false,
        stars: 0
    }
]

for(i = 0; i < movies.length; i++) {
    if(movies[i].watched === true) {
        console.log("He visto la película: " + movies[i].titulo + " - " + movies[i].stars + " stars");
    }else if(movies[i].watched === false) {
        console.log("No he visto la película: " + movies[i].titulo + " - " + movies[i].stars + " stars");
    }
}

//La forma de hacerlo con forEach:
movies.forEach(function(movie) {
    if(movie.watched === true) {
        console.log("He visto la película: " + movie.titulo + " - " + movie.stars + " stars");
    }else if(movie.watched === false) {
        console.log("No he visto la película: " + movie.titulo + " - " + movie.stars + " stars");
    }
})

//La forma en la que lo resolvió Colt es:
movies.forEach(function(movie) {
    var result = "I have ";
    if(movie.watched) {
        result += "seen ";
    }else {
        result += "not seen ";
    }
    result += "\"" + movie.titulo + "\" - ";
    result += movie.stars + " stars";
    console.log(result);
})