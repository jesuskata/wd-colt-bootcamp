var jg1Button = document.getElementById("jg1");
var jg2Button = document.getElementById("jg2");
var resetButton = document.getElementById("rs");
var sc1 = document.getElementById("sc1");
var sc2 = document.getElementById("sc2");
var gameTimes = document.getElementById("gm");
var numInput = document.querySelector("input");
var p1Score = 0;
var p2Score = 0;
var gameOver = false;
var gameWin = 5;

jg1Button.addEventListener("click", function() {
    if(!gameOver) {
        p1Score++;
        if(p1Score === gameWin) {
            gameOver = true;
            sc1.classList.add("winner");
        }
        sc1.textContent = p1Score;
    }
});

jg2Button.addEventListener("click", function() {
    if(!gameOver) {
        p2Score++;
        if(p2Score === gameWin) {
            gameOver = true;
            sc2.classList.add("winner");
        }
        sc2.textContent = p2Score;
    }
});

resetButton.addEventListener("click", function() {
    p1Score = 0;
    p2Score = 0;
    sc1.textContent = 0;
    sc2.textContent = 0;
    sc1.classList.remove("winner");
    sc2.classList.remove("winner");
    gameOver = false;
    numInput.value = "";
    gameWin = 5;
    gameTimes.textContent = gameWin;
});

numInput.addEventListener("change", function() {
    gameTimes.textContent = this.value;
    gameWin = Number(this.value);
    reset();
})

function reset() {
    p1Score = 0;
    p2Score = 0;
    sc1.textContent = 0;
    sc2.textContent = 0;
    sc1.classList.remove("winner");
    sc2.classList.remove("winner");
    gameOver = false;
}