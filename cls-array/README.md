# Clase: Array

## Podemos hacer un arreglo para incluir en él diferentes componentes
```javascript
var friends = ["Elo", "Aleisa", "Tiana", "Bebé"];
```

## Podemos llamar los componenetes del arreglo
```javascript
var friends = ["Elo", "Aleisa", "Tiana", "Bebé"];

friends[0]; //Elo
```

## Podemos actualizar los valores del arreglo
```javascript
var friends = ["Elo", "Aleisa", "Tiana", "Bebé"];

friends[0] = "Eloísa";
friends[2] = "Ashisa";
```

## Podemos agregar componentes al arreglo existente
```javascript
var friends = ["Elo", "Aleisa", "Tiana", "Bebé"];

friends[4] = "Alex";
```

## Podemos inicializar un arreglo vacío
```javascript
var friends = [];
var friends = new Array()
```

## Un arreglo puede contener toda cantidad y clase de datos
```javascript
var random_collection = [49, true, "Elo", null];
```

## Un arreglo puede tener la propiedad de lenght
```javascript
var num = [10, 12, 07];
num.lenght //3
```

## Algunas de las propiedades más importantes de los array
[Class: Colt Steel - Array Methods](http://webdev.slides.com/coltsteele/javascript-basics-20-21-62#/ target="_blank")
- **Push/Pop**
  - Push: agrega al final del arreglo
  ```javascript
  var colors = ["red", "dark gray", "white"];
  colors.push("green");
  //["red", "dark gray", "white", "green"]
  ```

  - Pop: elimina el último elemento del arreglo
  ```javascript
  var colors = ["red", "dark gray", "white"];
  colors.pop();
  //["red", "dark gray"]
  ```

- **Shift/Unshift**
  - Shift: agrega al inicio del arreglo
  ```javascript
  var colors = ["red", "dark gray", "white"];
  colors.unshift("green");
  //["green", "red", "dark gray", "white"]
  ```

  - Unshift: elimina el primer elemento del arreglo
  ```javascript
  var colors = ["red", "dark gray", "white"];
  colors.shift();
  //["dark gray", "white"]
  ```

- **indexOf:** según el argumento y la variable, busca dentro del arreglo y si lo enccuentra, devuelve su número de colocación
```javascript
var friends = ["Elo", "Aleisa", "Tiana", "Bebé", "Aleisa"];
// regresa el primer index en el cual se encuentre el argumento
friends.indexOf("Tiana"); //2
friends.indexOf("Aleisa"); //1, not 4
// regresa -1 en caso que el elemento buscado no se encuentre
friends.indexOf("Lupe"); //-1
```

- **Slice:** se usa para copiar partes de un arreglo
  - Podemos escoger los elementos a copiar del arreglo
  ```javascript
  var fruits = ["plátano", "mango", "limón", "manzana", "naranja"];
  var citrus = fruits.slice(2, 4); //citrus contiene ["limón", "naranja"]
  ```

  - Podemos copiar el arreglo completo
  ```javascript
  var num = [1, 2, 3];
  var otherNum = num.slice();
  // ambos arreglos son [1, 2, 3]
  ```