var h1 = document.querySelector("h1");

h1.style.color = "darkgray";
h1.style.border = "5px solid pink";

var p = document.querySelector("p");
//Con p.classList instrucción podemos ver lo que hay dentro del objeto
p.classList;
p.classList.add("big");
//Para remover la clase agregada tecleamos lo siguiente
p.classList.remove("big");
//Para asignar o dejar de asignar la clase se puede con lo siguiente (que cambia a true o a false)
p.classList.toggle("big");

//En esta parte del código vamos a manipular el texto con una función llamada textContent
var tag = document.getElementsByTagName("p")[0];

tag.textContent; //Nos va a mostrar el contenido de <p> sin los tags intermedios (<strong>): "Corgi mixes are super adorable"
tag.textContent = "Blah Blah Blah Blah";

//Otro modo de modificar el texto es con innerHTML, con el cual podemos adquirir de vuelta el contenido del texto con sus tags incluidos
tag.innerHTML; //Nos va a mostrar el contenido de <p> con los tags intermedios (<strong>): "Corgi mixes are <strong>super</strong> adorable"
tag.innerHTML = "Corgi mixes are <strong>super duper</strong> adorable";

//En esta sección vamos a hacer modificaciones a los atributos de los elementos que seleccionemos
var link = document.querySelector("a");
link.getAttribute; //"www.google.com"
//Vamos a cambiar el atributo href y a establecer el atributo target
link.setAttribute("href", "http://www.dogs.com");
link.setAttribute("target", "_blank");
document.querySelector("a").textContent = "Soy un Link que te lleva a Dogs";

var img = document.querySelector("img");
//Vamos a cambiar la imagen src
img.setAttribute("src", "img/logo-kata-sf.png");