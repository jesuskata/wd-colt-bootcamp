# Ejercicio: Array para ToDo List
### El código que se creó en javascript es:
```javascript
//Archivo JavaScript
var todos = [];

var input = prompt("Qué te gustaría hacer?");

while(input !== "quit") {
    if(input === "list") {
        console.log(todos);
    }else if(input === "new") {
        //pedir un nuevo todo
        var newTodo = prompt("Ingresa un nuevo Todo");
        //agregar al arreglo todo
        todos.push(newTodo);
    }
    //preguntar de nuevo
    var input = prompt("Qué te gustaría hacer?");
}
console.log("Ok, hemos terminado por hoy");
```

1. Lo primero que necesitamos es crear la variable todos vacía
```javascript
//Archivo JavaScript
var todos = [];
```
Con esto, lo que haremos será generar el arreglo principal en esta variable, y en ella agregaremos los elementos del todo

2. Necesitamos pedir al usuario que ingrese lo que desea de 3 opciones
```html
//Archivo HTML
    <li>"new" - Add a ToDo</li>
    <li>"list" - View All ToDos</li>
    <li>"quit" - Quit App</li>
```
entonces, a través de un prompt obtenemos la acción que desea realizar el usuario
```javascript
//Archivo JavaScript
var input = prompt("Qué te gustaría hacer?");
```

3. Ahora, establecemos con las condiciones pertinentes el actuar de nuestro programa ante la decisión que tome
```javascript
while(input !== "quit") {
    if(input === "list") {
        console.log(todos);
    }else if(input === "new") {
        //pedir un nuevo todo
        var newTodo = prompt("Ingresa un nuevo Todo");
        //agregar al arreglo todo
        todos.push(newTodo);
    }
    //preguntar de nuevo
    var input = prompt("Qué te gustaría hacer?");
}
console.log("Ok, hemos terminado por hoy");
```

### Listo, con esto tenemos el programa de ToDo List con HTML y JavaScript