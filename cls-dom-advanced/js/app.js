var button = document.querySelector("button");
var paragraph = document.querySelector("p");

//La sitanxis addEventListener hace que al ejecutar una acción (se da en el primer parámetro), realice o ejecute una función (dada en el segundo parámetro)
button.addEventListener("click", par);

function par() {
    paragraph.textContent = "Alguien me ha click-eado";
    paragraph.style.color = "darkgray";
}

var lis = document.querySelectorAll("li");

//En este ejemplo vemos como podemos mandar a llamar una función dentro del addEventListener; además, estamos usando por primera vez el this, con el cual hacemos referencia al elemento integrado en la función sin llamarlo por su nombre, sino con this
for(i = 0; i < lis.length; i++) {
    lis[i].addEventListener("click", function() {
        this.style.color = "pink";
    });
}

//NOTA: La recomendación de Colt es que la función la declaremos dentro del addEventListener cuando la vayamos a usar una sola vez dentro de esta propiedad. En caso de usarla en otras diferentes circunxtancias, es preferible darla de alta en una función como en el primer ejemplo (function par())