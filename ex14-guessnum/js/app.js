// Crear el numeroSecreto
var numeroSecreto = 4;

// adivina el numeroSecreto
var guess = Number(prompt("Adivina el número secreto"));

// checa el número
if(guess === numeroSecreto) {
    alert("Muy bien!, haz adivinado el núemro");
}else if(guess < numeroSecreto) {
    alert("Te falló, te haz ido un poco abajo del número");
}else if(guess > numeroSecreto) {
    alert("Te falló, te haz ido un poco arriba del número");
}else {
    alert("Vuelve a intentarlo, al parecer haz escrito un caracter y no un número");
}