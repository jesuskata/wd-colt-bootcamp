# Git

## Introduction

* What is git
* What is github
* Why should you care
* Novel writing analogy
* Installing git (Not really)

## Git Basics

* Git init
* Git status
* Git add
* Git commit

## Git Checkout

* Git log
* Git checkout
* git revert --no-commit 2ht487..HEAD

    HEAD
    0 -> 0 -> 0 -> 0
                   Master