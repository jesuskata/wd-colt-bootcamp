var express = require('express');
var app = express();

app.get("/", function(req, res) {
    res.send("Bienvenido a mi Ejercicio de Express");
});
//Esta es mi solución
/*app.get("/speak/:animal", function(req, res) {
    var whatAnimal = req.params.animal;
    if(whatAnimal === "pig") {
        res.send("The " + whatAnimal + " say 'Oink!'");
    }else if(whatAnimal === "cow") {
        res.send("The " + whatAnimal + " say 'Mooo!'");
    }else if(whatAnimal === "dog") {
        res.send("The " + whatAnimal + " say 'Woof Woof!'");
    }else {
        res.send("Lo siento, página no encontrada...Que tal, cómo estás?");
    }
});*/

//Esta es la solución larga de Colt Steele
/*app.get("/speak/:animal", function(req, res) {
    var whatAnimal = req.params.animal;
    var sound = "";
    if(whatAnimal === "pig") {
        sound = "'Oink!'";
    }else if(whatAnimal === "cow") {
        sound = "'Mooo!'";
    }else if(whatAnimal === "dog") {
        sound = "'Mooo!'";
    }
    res.send("The " + whatAnimal + " say " + sound);
});*/

//Esta es la solución eficiente de Colt Steele
app.get("/speak/:animal", function(req, res) {
    var sounds = {
        pig: "'Oink!'",
        cow: "'Mooo!'",
        dog: "'Woof Woof!'",
        cat: "'Meoh!'",
        turtle: "'Bloop Bloop!'"
    };
    var whatAnimal = req.params.animal.toLowerCase();
    var sound = sounds[whatAnimal];
    res.send("The " + whatAnimal + " say " + sound);
});

//Esta es mi solución
/*app.get("/repeat/:frase/:num", function(req, res) {
    var whatFrase = req.params.frase;
    var whatNum = parseInt(req.params.num);
    for(var i = 0; i < whatNum; i++) {
        res.write(whatFrase + " ");
    }
    res.end();
});*/

//Esta es la solución de Colt Steele
app.get("/repeat/:frase/:num", function(req, res) {
    var whatFrase = req.params.frase;
    var whatNum = Number(req.params.num);
    var result = "";
    for(var i = 0; i < whatNum; i++) {
        result += whatFrase + " ";
    }
    res.send(result);
});

app.get("*", function(req, res) {
    res.send("Lo siento, página no encontrada...Que tal, cómo estás?");
});

app.listen(3000, function() {
    console.log("El servidor ha iniciado");
});