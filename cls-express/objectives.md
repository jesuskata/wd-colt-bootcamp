## Introduction to Express

* What is a Framework? How is it diferent from a library?
* What is Express?
* Why are we using Express?

## Our First Express App

* Review an existing app (DogDemo)
* Review HTTP response/request lifecycle
* Create our own simple Express App

## NPM Init and Package.json

* Use the `--save` flag to install packages
    * Una vez que hemos creado un package.json con la instrucción npm init, al instalar nuestras librerías o Frameworks agregando al final --save, estos se irán añadiendo al package.json que hemos creado
* Explain what the package.json file does
* Use `npm init` to create a new package.json

## More Routing

* Show the `*` router matcher
* Write routes containing route parameters
* Discuss route order
    * Es muy importante el orden de las peticiones, ya que * viene significando todas las peticiones, por lo que si se coloca al inicio, solamente estaremos obteniendo respuesta del `*` y no de las demás peticiones get