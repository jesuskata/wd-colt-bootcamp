var express = require('express');
var app = express();

// "/" => "Hi there"
app.get("/", function(req, res) {
    console.log("Alguien hizo una petición a /")
    res.send("Hi there")
});

// "/bye" => "Goodbye!"
app.get("/bye", function(req, res) {
    console.log("Alguien hizo una petición a /bye")
    res.send("Goodbye!")
});

// "/dog" => "Hi!"
app.get("/dog", function(req, res) {
    console.log("Alguien hizo una petición a /dog")
    res.send("Hi!")
});


app.get("/r/:subredditName", function(req, res) {
    // Con req.params el sistema nos detecta el enlace o palabra guardada en subredditName
    var subreddit = req.params.subredditName;
    // console.log(req.params);
    // con toUpperCase() hacemos que la palabra que lo contenga se convierta toda a mayúsculas
    res.send("Bienvenido a la página de " + subreddit.toUpperCase());
});

app.get("/r/:subredditName/comments/:id/:title/",function(req, res) {
    console.log(req.params);
    res.send("Bienvenido a la página de COMENTARIOS");
});

// "/peticion-inexistente" => "Mensaje de ERROR personalizado", esta función es muy útil para ofrecer una página Error preconfigurada con un mensaje o con una plantilla html especial
app.get("*", function(req, res) {
    console.log("Alguien hizo una petición equivocada");
    res.send("Eres toda una estrella")
})

//En cloud9, tenemos que hacerlo de la manera siguiente (cambiando 3000 por process.env.PORT y cambiando la function por process.env.IP)
/*app.listen(process.env.PORT, process.env.IP, function() {*/
app.listen(3000, function() {
    console.log("El servidor ha iniciado")
});