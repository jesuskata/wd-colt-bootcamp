var colors = ["rojo", "morado", "azul", "amarillo"];

console.log("Se van a imprimir los colores del arreglo colors")
for(var i = 0; i < colors.length; i++) {
    console.log(colors[i]);
}

var colors = ["rojo", "morado", "azul", "amarillo"];

colors.forEach(function(color) {
    console.log("Este es uno de los colores del arreglo: " + color);
})

var colors = ["rojo", "morado", "azul", "amarillo"];

function printColor(color) {
    console.log("----------");
    console.log(color);
    console.log("----------");
}

colors.forEach(printColor);