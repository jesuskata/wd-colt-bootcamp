# Array Iteration
[Class: Colt Steel - Array Iteration](http://webdev.slides.com/coltsteele/javascript-basics-20-21-22-63#/)

A continuación veremos un ejemplo de cómo podemos interactuar con un arreglo y obtener sus elementos
* Lo primero que haremos será declarar la variable que contendrá el arreglo
```javascript
var colors = ["rojo", "morado", "azul", "amarillo"];
```

* Vamos a hacer el primer llamado de los elementos del arreglo con un loop: `for`
```javascript
console.log("Se van a imprimir los colores del arreglo colors")
for(var i = 0; i < colors.length; i++) {
    console.log(colors[i]);
}
```

En este ejemplo podemos ver que con el flag 'length' obtenemos la cantidad de elementos que están dentro del arreglo, por lo tanto nos sirve como límite del `for`

* Ahora veremos cómo podemos lograr lo mismo pero con un loop de for llamado `forEach`
```javascript
var colors = ["rojo", "morado", "azul", "amarillo"];
//Es importante recordar que lo que va dentro del paréntesis es una función cualquiera, en este caso, declarándola ahí mismo
colors.forEach(function(color) {
    console.log("Este es uno de los colores del arreglo: " + color);
})
```

Como podemos observar, es más sencillo el uso de `forEach`, ya que con la función podemos crear un parámetro cualquiera que se relacionará con los elementos del array declarado. Y con el 'console.log' llamamos a los elementos exactos contenidos en él

* Por último, en este otro ejemplo vemos como podemos llamar una función declarada con anterioridad (recordar que al llamarla no lleva paréntesis)
```javascript
var colors = ["rojo", "morado", "azul", "amarillo"];
function printColor(color) {
    console.log("----------");
    console.log(color);
    console.log("----------");
}
colors.forEach(printColor);
```