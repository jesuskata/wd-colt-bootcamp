var express = require("express");
var app = express();
var request = require("request");
app.set("view engine", "ejs");

app.get("/", function(req, res) {
    res.render("search");
});

app.get("/results", function(req, res) {
    //console.log(req.query.search); //Con esto vemos lo que se está escribiendo en el formulario search
    var title = req.query.search;
    var url = 'http://omdbapi.com/?s=' + title + '&apikey=thewdb';
    request(url, function(error, response, body) {
    if(!error && response.statusCode === 200) {
        //res.send(body); //Con esto vemos el string completo y podemos elegir qué ver o buscar
        var data = JSON.parse(body); //Con esta instrucción convertimos el string en un objeto JavaScript
        //res.send(results["Search"][0]["Title"]); //Es importante considerar aquí las letras contenidas en la variable a buscar, en este caso Search inicia con mayúscula
        res.render("results", {data: data});
    }
    });
});

//app.listen(process.env.PORT, process.env.IP, function() {
app.listen(3000, function() {
    console.log("Movie App inicializada");
});