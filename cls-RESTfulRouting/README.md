# RESTful Routing

## Introduction
* Define REST and explain WHY it matters
* List all 7 RESTful routes
* Show example of RESTful routing in practice

## Blog Index
* Setup the Blog App
* Create the Blog model
* Add INDEX route and template
* Add Simple Nav Bar

## Basic Layout
* Add Header and Footer Partials
* Include Semantic UI
* Add Simple Nav

## SHOWtime
* Add Show route
* Add Show template
* Add links to show page
* Style show template

## Edit/Update
* Add Edit route
* Add Edit form
* Add Update route
* Add Update form
* Add Method-Override
    * Este método es muy importante para lograr que se puedan realizar las acciones PUT y DELETE en la app, debido a que por naturaleza HTML no lo permite por cuestiones que aún no se terminan de definir (Checar: https://softwareengineering.stackexchange.com/questions/114156/why-are-there-are-no-put-and-delete-methods-on-html-forms), además, es importante mencionar que tanto ruby, python como los otros lenguajes de programación tienen sus trucos para lograr el resultado esperado

## Final Updates
* Sanitize blog body
    * Con esto, eliminamos la probabilidad de que se introduzca al textbox etiquetas tipo script, dejando únicamente las etiquetas html
* Style Index
* Update REST Table