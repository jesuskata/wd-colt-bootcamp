/*var button = document.querySelector("button");
var body = document.querySelector("body");
var isOrange = true;
    
button.addEventListener("click", function() {
    if(isOrange) {
        body.style.background = "orange";
    }else {
        body.style.background = "none";
    }
    isOrange = !isOrange;
});*/

//Este es el código utilizado por Colt en la solución
/*var button = document.querySelector("button");
var isOrange = false;


button.addEventListener("click", function() {
    if(isOrange) {
        document.body.style.background = "white";
    }else {
        document.body.style.background = "orange";
    }
    isOrange = !isOrange;
});*/

//Esta es la segunda forma de resolver el ejercicio
var button = document.querySelector("button");

button.addEventListener("click", function() {
    document.body.classList.toggle("orange");
});