var express               = require("express"),
    mongoose              = require("mongoose"),
    passport              = require("passport"),
    bodyParser            = require("body-parser"),
    User                  = require("./models/user"),
    LocalStrategy         = require("passport-local"),
    passportLocalMongoose = require("passport-local-mongoose")

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/auth_demo_app");

var app =  express();
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));
//Esta sección debe ir después de llamar app, de lo contrario habrá un error al no existir la variable app requerida
app.use(require("express-session")({
    secret: "Todo lo puedo en Cristo que me fortalece", //Este texto es necesario para decodificar la info, y puede ser cualquier texto que deseemos
    resave: false,
    saveUninitialized: false
}));

//Estos dos métodos son sumamente importantes en passport. Son los responsables de leer la sesión tomando los datos de la sesión que está codeada, descodeándola y volviéndola a asegurar una vez que se terminó la sesión. Al agregar passportLocalMongoose en nuestro archivo user.js, automáticamente está utilizando estos dos métodos
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//Las dos líneas siguientes son indispensables siempre que queramos usar passport en nuestra aplicación
app.use(passport.initialize());
app.use(passport.session());
// caching disabled for every route
app.use(function(req, res, next) {
    res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
});

//============
// ROUTES
//============

app.get("/", function(req, res) {
    res.render("home");
});

app.get("/secret", isLoggedIn, function(req, res) {
    res.render("secret");
});

// Add Routes
// Register form
app.get("/register", function(req, res) {
    res.render("register");
});

// Handling user signup
app.post("/register", function(req, res) {
    req.body.username
    req.body.password
    User.register(new User({username: req.body.username}), req.body.password, function(err, user) {
        if(err) {
            console.log(err);
            return res.render('register');
        }
        passport.authenticate("local")(req, res, function() {
        res.redirect("/secret");
        });
    });
});

// Login form
app.get("/login", function(req, res) {
    res.render("login");
});

// Login logic
// Middleware, hace referencia al passport.authenticate como segundo parámetro del app.post, el cual se realiza antes de ejecutar la función (al ser un login, tiene que comprobar que el usuario esté previamente registrado y que esté correcta su información de usuario y contraseña)
app.post("/login", passport.authenticate("local", {
    successRedirect: "/secret",
    failureRedirect: "/login"
}), function(req, res) {
});

// Logout
app.get("/logout", function(req, res) {
    req.logout();
    res.redirect("/")
});

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.redirect("/login");
};

//app.listen(process.env.PORT, process.env.IP, function() {
app.listen(3000, function() {
    console.log("Server inicializado");
});