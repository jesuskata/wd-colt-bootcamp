var Campground = require("../models/campground");
var Comment = require("../models/comment");

// All the middlewares goes here

var middlewareObj = {};

middlewareObj.checkCampgroundOwnership = function(req, res, next) {
    if(req.isAuthenticated()) {
        Campground.findById(req.params.id, function(err, foundCampground) {
            if(err) {
                req.flash("error", "No fue encontrado el campground");
                res.redirect("back");
            } else {
                // Does user own the campground?
                //console.log(foundCampground.author.id); // No son lo mismo, este primero es un objeto mongoose
                //console.log(req.user._id); // No son lo mismo, este segundo es un string
                if(foundCampground.author.id.equals(req.user._id)) {
                    next();
                } else {
                    req.flash("error", "Lo sentimos. Pero no tienes permiso para hacer eso");
                    res.redirect("back");
                }
            }
        });
    } else {
        req.flash("error", "Es necesario logearte primero");
        res.redirect("back");
    }
};

middlewareObj.checkCommentOwnership = function(req, res, next) {
    if(req.isAuthenticated()) {
        Comment.findById(req.params.comment_id, function(err, foundComment) {
            if(err) {
                res.redirect("back");
            } else {
                // Does user own the comment?
                //console.log(foundComment.author.id); // No son lo mismo, este primero es un objeto mongoose
                //console.log(req.user._id); // No son lo mismo, este segundo es un string
                if(foundComment.author.id.equals(req.user._id)) {
                    next();
                } else {
                    req.flash("error", "Lo sentimos. Pero no tienes permiso para hacer eso");
                    res.redirect("back");
                }
            }
        });
    } else {
        req.flash("error", "Es necesario logearte primero");
        res.redirect("back");
    }
};

middlewareObj.isLoggedIn = function(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    req.flash("error", "Es necesario logearte primero");
    res.redirect("/login");
};

module.exports = middlewareObj;