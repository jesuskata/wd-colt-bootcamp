## YelpCamp

* Add a Landing Page
* Add Campgrounds Page that lists all campgrounds

Each campground has:

* Name
* Images

## Layout and Basic Styling

* Create our header and footer partials
* Add in Bootstrap

## Creating New Campgrounds

* Setup new campgrounds POST route
* Add in body-parser
* Setup route to show form
* Add basic unstyled form

## Style the campgrounds page

* Add a better header7title
* Make campgrounds display in a grid

## Style th navbar and Form

* Add a navbar to all templates
* Style the new campground form

## Add Mongoose

* Install and configure mongoose
* Setup campground model
* Use campground model inside our routes!

## Show Page

* Review the RESTful routes we've seen so far
* Add description to our campground model
* Show db.collection.drop()
* Add a show route/template

RESTFUL ROUTES

name     url          verb   description
========================================================
INDEX    /dogs        GET    Display a list of all dogs
NEW      /dogs/new    GET    Display form to make a new dog
CREATE   /dogs        POST   Add new dog to DB
SHOW     /dogs/:id    GET    Shows info about one dog

## Refactor Mongoose Code

* Create a models directory
* Use module.exports
* Requiere everithing correctly

## Add Seed Files

* Add a seeds.js file
* Run the seeds file every time the server starts

## Add the Comment Model

* Make our errors go away
* Display comments on campgrounds show page

## Comment New/Create

* Discuss nested routes
* Add the comment new add create routes
* Add the new comment form

RESTFUL ROUTES

name     url          verb   description
========================================================
INDEX    /dogs        GET    Display a list of all dogs
NEW      /dogs/new    GET    Display form to make a new dog
CREATE   /dogs        POST   Add new dog to DB
SHOW     /dogs/:id    GET    Shows info about one dog

## Style Show Page

* Add sidebar to show page
* Display comments nicely

## Finishing Styling Show Page

* Add public directory
* Add custom stylesheet

## Auth Pt 1 - User Model

* Install all packages needed for auth
* Define User model

## Auth Pt 2 - Register

* Configure passport
* Add register routes
* Add register template

## Auth Pt 3 - Login

* Add login routes
* Add login template

## Auth Pt 4 - Logout/Navbar

* Add logout route
* Prevent user from adding a comment if not signed in
* Add links to navbar

## Auth Pt 5 - Show/Hide Links

* Show/Hide auth links correctly

## Refactor The Routes

* Use Express router to reorganize all routes

## Users + Comments

* Associate users and comments
* Save author's name to a comment automatically

## Users + Campgrounds

* Prevent an unauthenticated user form creating a campground
* Save username+id to newly created campground

## Editing Campgrounds

* Add Method-Override
* Add Edit Route for Campgrounds
* Add link to Edit Page
* Add Update Route
* Fix $set problem

## Editing Campgrounds

* Add Destroy Route
* Add Delete Button

## Authorization Pt 1: Campgrounds

* User can only edit his/her campground
* User can only delete his/her campground
* Hide/Show edit and delete buttons

## Editing Comments

* Add Edit route for comments
* Add Edit button
* Add Update route

Campground Edit Route: <!--/campground/:id/edit-->
Comment Edit Route: <!--/campground/:id/comments/:comment_id/edit-->

## Deleting Comments

* Add Destroy route
* Add Delete button

Campground Destroy Route: <!--/campground/:id/-->
Comment Destroy Route: <!--/campground/:id/comments/:comment_id/-->

## Authorization Comments

* User can only edit his/her comments
* User can only delete his/her comments
* Hide/Show edit and delete buttons
* Refactor middleware

## Adding in Flash

* Demo working version
* Install and configure connect-flash
* Add bootstrap alerts to header

RESTFUL ROUTES

name     url          verb   description
========================================================
INDEX    /dogs        GET    Display a list of all dogs
NEW      /dogs/new    GET    Display form to make a new dog
CREATE   /dogs        POST   Add new dog to DB
SHOW     /dogs/:id    GET    Shows info about one dog