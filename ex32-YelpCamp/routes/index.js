var express = require("express");
var router = express.Router();
var passport = require("passport");
var User = require("../models/user");
var middleware = require("../middleware"); // Al haber nombrado al archivo dentro de la carpeta index.js, en automático es lo primero que busca el sistema para relacionarlo con el require

// Root Route
router.get("/", function(req, res) {
    res.render("landing");
});

// Register Form Route
router.get("/register", function(req, res) {
    res.render("register");
});

// Sign Up logic
router.post("/register", function(req, res) {
    var newUser = new User({username: req.body.username});
    User.register(newUser, req.body.password, function(err, user) {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            return res.redirect("/register");
        }
        passport.authenticate("local")(req, res, function() {
            req.flash("success", "Bienvenido(a) a Yelpcamp " + user.username);
            res.redirect("/campgrounds");
        });
    });
});

// Login Form Route
router.get("/login", function(req, res) {
    res.render("login");
});

// Login logic
// Esta es la forma en que está escrito el código siguiente: router.post("/login", middleware, callback);
router.post("/login", passport.authenticate("local",
    {
        successRedirect: "/campgrounds",
        failureRedirect: "/login"
    }), function(req, res) {
});

// Logout Route
router.get("/logout", function(req, res) {
    req.logout();
    req.flash("success", "Has cerrado tu sesión con éxito");
    res.redirect("/campgrounds")
});

module.exports = router;