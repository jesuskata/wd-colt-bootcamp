var mongoose = require("mongoose");
var Campground = require("./models/campground");
var Comment = require("./models/comment");

var data = [
    {
        name: "Playa",
        image: "https://static.pexels.com/photos/137588/pexels-photo-137588.jpeg",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac mi hendrerit, luctus ipsum lacinia, efficitur neque. Nunc lacinia quam massa, quis blandit nibh maximus id. Sed a turpis nec metus dapibus sagittis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam sit amet justo sit amet massa venenatis maximus ac consequat est. Vivamus rhoncus orci mi, id tristique augue hendrerit dignissim. Integer eu leo dolor. Nam orci augue, laoreet sit amet nunc eget, iaculis faucibus erat. Etiam dictum eros eu scelerisque molestie. Nulla vitae mauris sit amet massa varius facilisis."
    },
    {
        name: "Pirámides",
        image: "https://static.pexels.com/photos/160201/pexels-photo-160201.jpeg",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac mi hendrerit, luctus ipsum lacinia, efficitur neque. Nunc lacinia quam massa, quis blandit nibh maximus id. Sed a turpis nec metus dapibus sagittis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam sit amet justo sit amet massa venenatis maximus ac consequat est. Vivamus rhoncus orci mi, id tristique augue hendrerit dignissim. Integer eu leo dolor. Nam orci augue, laoreet sit amet nunc eget, iaculis faucibus erat. Etiam dictum eros eu scelerisque molestie. Nulla vitae mauris sit amet massa varius facilisis."
    },
    {
        name: "Comedores",
        image: "https://static.pexels.com/photos/5317/food-salad-restaurant-person.jpg",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac mi hendrerit, luctus ipsum lacinia, efficitur neque. Nunc lacinia quam massa, quis blandit nibh maximus id. Sed a turpis nec metus dapibus sagittis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam sit amet justo sit amet massa venenatis maximus ac consequat est. Vivamus rhoncus orci mi, id tristique augue hendrerit dignissim. Integer eu leo dolor. Nam orci augue, laoreet sit amet nunc eget, iaculis faucibus erat. Etiam dictum eros eu scelerisque molestie. Nulla vitae mauris sit amet massa varius facilisis."
    }
];

function seedDB() {
    //Remove all campgrounds
    Campground.remove({}, function(err) {
        if(err) {
            console.log(err);
        }
        console.log("Remove Campgrounds");
        //add a few campgrounds
        data.forEach(function(seed) {
            Campground.create(seed, function(err, campground) {
                if(err) {
                    console.log(err);
                } else {
                    console.log("Added a campground");
                    //create a comment
                    Comment.create(
                        {
                            text: "This place is great, but I wish there was Internet",
                            author: "Alex"
                        }, function(err, comment) {
                            if(err) {
                                console.log(err);
                            } else {
                                campground.comments.push(comment);
                                campground.save();
                                console.log("Created new comment");
                            }
                        });
                }
            });
        });
    });
    //add a few comments
};

module.exports = seedDB;