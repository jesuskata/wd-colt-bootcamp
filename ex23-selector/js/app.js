//Declarar 4 diferentes maneras de seleccionar el primer <p>
var p = document.querySelector("p");
console.log(p);

var p = document.querySelector("#first");
console.log(p);

var p = document.querySelector(".special");
console.log(p);

var p = document.querySelector("p#first.special");
console.log(p);

var p = document.querySelectorAll("p")[0];
console.log(p);

var p = document.querySelectorAll("#first");
console.log(p);

var p = document.querySelectorAll(".special")[0];
console.log(p);

var p = document.getElementById("first");
console.log(p);

var p = document.getElementsByClassName("special")[0];
console.log(p);

var p = document.getElementsByTagName("p")[0];
console.log(p);

var p = document.querySelector("h1 + p");
console.log(p);